package com.happy.interceptors;

import com.happy.entity.Result;
import com.happy.entity.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 请求拦截器，检查token 以及在response中添加两个头信息，让浏览器知道允许跨域。
 */
@Slf4j
public class UserInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        log.info("user 拦截器启动------------------");
        response.setCharacterEncoding("UTF-8");
        String origin = request.getHeader("Origin");
        response.setHeader("Access-Control-Allow-Origin", origin);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        Object loginInfo = request.getSession().getAttribute("loginInfo");
        Object admin = request.getSession().getAttribute("admin");
        if (loginInfo == null && admin == null) {  //两个都为null 说明 普通用户身份和管理员身份都为登录，那么 让他先去登录
            response.getWriter().write(new Result(ResultEnum.tokenExpired).toString());
            return false;
        }
        return true;
    }
}
