package com.happy.interceptors;

import com.happy.entity.Result;
import com.happy.entity.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class AdminInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        log.info("admin 拦截器启动------------------");
        response.setCharacterEncoding("UTF-8");
        String origin = request.getHeader("Origin");
        response.setHeader("Access-Control-Allow-Origin", origin);
        response.setHeader("Access-Control-Allow-Credentials","true");
        Object loginInfo = request.getSession().getAttribute("admin");
        if(loginInfo==null){
            response.getWriter().write(new Result(ResultEnum.tokenExpired).toString());
            return false;
        }
        String timeStamp = loginInfo.toString().split("_")[1];
        long time = 0L;
        try {
            time = Long.parseLong(timeStamp);
        }catch (Exception e){
            e.printStackTrace();
        }
        if((System.currentTimeMillis()-time)>5*60*1000){  //时间大于5分钟 登录过期
            response.getWriter().write(new Result(ResultEnum.tokenExpired).toString());
            return false;
        }else {
            return true;
        }
    }
}
