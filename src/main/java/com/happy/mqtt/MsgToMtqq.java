package com.happy.mqtt;

import com.google.gson.Gson;

import java.util.HashMap;

public class MsgToMtqq {
    //msg_id 的取值
    private int CONTROL_IO = 1000;
    private int CONFIG_WRITRDGCHANNEL = 1001;
    private int CONFIG_READDGCHANNEL = 1002;
    private int conditionRead = 1003;
    private int LoginWrite = 1004;
    private int timerWrite = 1005;
    private int getIoStatus = 1006;
    private int clearCondition = 1007;
    private int setperiod = 1008;
    private int getperiod = 1009;

    // msg_type的取值
    private String GET = "get";
    private String SET = "set";

    //Services 的取值
    private String clear;
    private String piroed;
    private String isvalue;
    private String io;
    private String config;
    private String timer;
    private String logic;
    private String adata;

    //io的取值
    private byte turnOnAll = 0;
    private byte turnOffAll = 0xFFFFFFFF;

    //isvalue的取值
    private String PIROED = "piroed";
    private String IO = "io";
    private String DATA = "data";
    private String CONFIG = "config";
    private String CONDITION = "condition";

    private String set_configJsonString(int msg_id, String action, String value) {
        Gson gson = new Gson();
        HashMap<String, Object> msgJson = new HashMap();
        HashMap<String, Object> ioJson = new HashMap<>();

        ioJson.put("action", "value");
        msgJson.put("servicers", ioJson);
        msgJson.put("msg_id", "msg_id");
        msgJson.put("mst_type", "set");
        String s = gson.toJson(msgJson);
        return s.replace("\n", "").replace("\t", "").replace(" ", "");
    }
}
