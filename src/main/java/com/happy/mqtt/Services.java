package com.happy.mqtt;

public class Services {
    Data data;
    private String clear;
    private String piroed;
    private String isvalue;
    private String config;
    private String timer;
    private String logic;

    @Override
    public String toString() {
        return "{" +
                "data=" + data +
                ", clear='" + clear + '\'' +
                ", piroed='" + piroed + '\'' +
                ", isvalue='" + isvalue + '\'' +
                ", config='" + config + '\'' +
                ", timer='" + timer + '\'' +
                ", logic='" + logic + '\'' +
                '}';
    }
}
