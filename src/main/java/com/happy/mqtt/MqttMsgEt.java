package com.happy.mqtt;

public class MqttMsgEt {
    String msg_type;
    String msg_id;
    Services services;

    @Override
    public String toString() {
        return "{" +
                "msg_type='" + msg_type + '\'' +
                ", msg_id='" + msg_id + '\'' +
                ", services=" + services +
                '}';
    }
}
