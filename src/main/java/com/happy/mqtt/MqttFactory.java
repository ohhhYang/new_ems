package com.happy.mqtt;

import com.google.gson.Gson;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class MqttFactory {
    @Value("${mqtt.host}")
    private String host;
    @Value("${mqtt.clientId}")
    private String clientId;
    @Value("${mqtt.username}")
    private String username;
    @Value("${mqtt.password}")
    private String password;
    @Value("${mqtt.timeout}")
    private int timeout;
    @Value("${mqtt.keepalive}")
    private int keepalive;
    private final JdbcTemplate jdbcTemplate;
    public MqttFactory(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Bean
    public MyMqttClient myMqttClient() {
        MyMqttClient myMqttClient = new MyMqttClient(host, username, password, clientId, timeout, keepalive);
        for (int i = 0; i < 5; i++) {
            try {
                myMqttClient.connect();
                final String sql = "insert into ems_data values(?, ? ,CURRENT_TIMESTAMP, ?, ?,?)";
                final Gson gson = new Gson();
                MyMqttClient.subscribe("push/+", 1, new IMqttMessageListener() {
                    private String userOfThisData;
                    private String sensorAddr;

                    @Override
                    public void messageArrived(String topic, MqttMessage message) throws Exception {
                        System.out.println(topic);
                        System.out.println(message.toString());

                        MqttMsgEt mqttMsgEt = null;
                        try {
                            mqttMsgEt = gson.fromJson(message.toString(), MqttMsgEt.class);
                        } catch (Exception e) {
                            System.out.println("Gson 转换失败！");
                        }
                        if (mqttMsgEt == null) {
                            System.out.println("mqttMsgEt为null！");
                            return;
                        }
                        try {
                            sensorAddr = topic.split("/")[1];
                        } catch (Exception e) {
                            sensorAddr = "nobody";
                            System.out.println("切割topic失败， topic: "+topic);
                        }
                        String sql2 = "select ems_device_user_id from ems_device where ems_device_address=?";
                        List<Map<String, Object>> lists = jdbcTemplate.queryForList(sql2, sensorAddr);
                        if (lists.size() != 0) {
                            Object ems_device_user_id = lists.get(0).get("ems_device_user_id");
                            userOfThisData = ems_device_user_id.toString();
                        } else {
                            userOfThisData = "nobody";
                        }
                        System.out.println("开始存放");
                        try {
                            int result = jdbcTemplate.update(sql, mqttMsgEt.services.data.toString(), mqttMsgEt.msg_type, sensorAddr, mqttMsgEt.msg_id, userOfThisData);
                            System.out.println("存放结束");
                            System.out.println("result : " + result);
                        }catch (Exception e){
                            System.out.println("存放失败"+mqttMsgEt.toString());
                        }
                    }
                });
                System.out.println("已经订阅了所有 cid");
                return myMqttClient;
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}