package com.happy.utils;

import javax.servlet.http.HttpServletRequest;

public class CheckToken {

    public static boolean check(HttpServletRequest request, String token) {
        Object to = request.getSession().getAttribute("token");
        if (to == null) {
            System.out.println("token验证不通过");
            to = "0_0";  //这个符号表情不错哦  // 防止空指针异常，给他一个无法从数据库校验成功的值，妥妥的。
        }
        System.out.println("前端给的： "+token+"\n 后端的："+to.toString());
        return token != null && token.equals(to.toString());
    }
}
