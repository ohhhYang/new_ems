package com.happy.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class OuyangUtils {

    public static String getPhoneFromRequest(HttpServletRequest request) {
        Object phone=request.getSession().getAttribute("loginInfo"); //可能为null
        String strPhone="";
        try{
            strPhone = phone.toString().split("_")[0];
        }catch (Exception e){
            e.printStackTrace();
            log.error("从loginInfo获取phone失败！");
            return null;
        }
        return strPhone;
    }
    public static boolean isEmptyOrNull(String s){
        if(s==null){
            return true;
        }
        s=s.trim();
        return s.equals("");
    }
    public static boolean isPhoneNum(String phone) {
        Pattern pattern = Pattern.compile("1[35789][0-9]{9}"); //定义手机号码正则表达式：11位数字、13、15、17、18、19开头
        if (phone == null) {                                //             判空      //
            return false;
        }
        if(phone.startsWith("888")){ //888开头测试账号 无需正则校验，方便调试
            return true;
        }
        phone = phone.trim();                        //去掉首尾段空字符
        Matcher matcher = pattern.matcher(phone);   //正则匹配
        return matcher.matches(); //返回匹配结果 true/false
    }
}
