package com.happy;

import com.happy.interceptors.AdminInterceptor;
import com.happy.interceptors.UserInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Import(SwaggerConfig.class)
@SpringBootApplication
public class OuyangApplication {
    public static void main(String[] args) {
        SpringApplication.run(OuyangApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer WebMvcConfigurer () {
        return new WebMvcConfigurer () {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/").allowedMethods("GET", "POST").allowCredentials(true);
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(new UserInterceptor()).addPathPatterns("/user/**","/sensor/**")
                .excludePathPatterns("/user/getToken");//这里被自己坑了，当时少写了个*，不能匹配到/下所有api，而自己却一直以为是拦截器没起作用，，，唉  吃一些长一智。
                registry.addInterceptor(new AdminInterceptor()).addPathPatterns("/admin/**")
                        .excludePathPatterns("/admin/getToken");
            }
        };
    }

}
