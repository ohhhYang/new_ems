package com.happy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket userDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("用户")
                .select()
                .paths(regex("/user.*"))
                .build();
    }

    @Bean
    public Docket adminDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("admin")
                .select()
                .paths(regex("/admin.*"))
                .build();
    }
    @Bean
    public Docket sensorDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("控制传感器")
                .select()
                .paths(regex("/sensor.*"))
                .build();
    }


    @Deprecated
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("环境检测系统接口测试平台")
                .description("参数含义请看源码")
                .termsOfServiceUrl("")
                .contact("无名之辈 欧阳")
                .version("1.0")
                .build();
    }



}
