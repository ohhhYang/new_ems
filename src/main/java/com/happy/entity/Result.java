package com.happy.entity;

import com.alibaba.fastjson.annotation.JSONType;
import lombok.Data;


@Data
@JSONType(includes = {"code", "msg", "data"})
public class Result {
    private String code;
    private String msg;
    private Object data;

    @Override
    public String toString() {
        return "{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public Result(ResultEnum resultEnum, Object data) {
        this.code = resultEnum.code;
        this.msg  = resultEnum.msg;
        this.data = data;
    }

    public Result(ResultEnum resultEnum) {
        this.msg = resultEnum.msg;
        this.code = resultEnum.code;
    }

}
