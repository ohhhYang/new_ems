package com.happy.entity;

public enum ResultEnum {

    sqlError("12","sql执行出错，数据库连接上了么？"),
    success("1", "操作成功"),
    authFailed("06", "用户名或者密码错误"),
    serverError("02","服務器忙，請稍後再試。"),
    tokenExpired("03","你还没有登陆。请先登陆"),
    controlCodeError("05","操作码错误，只能取0或1"),
//    addrNotExist("07","传感器地址不存在"),
    sensorFreezed("08","该传感器已经被冻结，请联系管理员"),
    paramNull("09","参数不能为空"),
    phoneFormatError("10","手机号格式错误"),
    sensorNotExist("11","该传感器不存在");

    public String code;
    public String msg;

    ResultEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}

