package com.happy.controller;

import com.happy.entity.Result;
import com.happy.entity.ResultEnum;
import com.happy.service.SensorService;
import com.happy.utils.OuyangUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sensor/")
public class Sensor {

    private final SensorService sensorService;
    public Sensor(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @PostMapping("getSensors") //通过用户手机号，查询绑定的传感器。
    public Result getSensors(HttpServletRequest request) {
        String phone = OuyangUtils.getPhoneFromRequest(request);
        if(phone==null){
            return new Result(ResultEnum.tokenExpired);
        }
        List sensors = sensorService.getSensors(phone);
        if (sensors == null) {
            return new Result(ResultEnum.serverError);
        }
        return new Result(ResultEnum.success,sensors);
    }

    @PostMapping("turnOnOffSensor")
    public Result turnOn_OffSensor(@RequestParam String sensorAddr, @RequestParam String controlCode, HttpServletRequest request) {
        String phone = OuyangUtils.getPhoneFromRequest(request);
        if (phone==null){
            return new Result(ResultEnum.tokenExpired);
        }
        switch (sensorService.validateSensorAddr(phone,sensorAddr)) { //权限、传感器地址校验
            case SensorService.SensorFreezed:
                return new Result(ResultEnum.sensorFreezed);
            case SensorService.SensorNotExist:
                return new Result(ResultEnum.sensorNotExist);
            default:
                return sensorService.turnOn_OffSensor(sensorAddr, controlCode);//service层只会返回三种结果，排除前两种异常情况，剩下的必定是sucess
        }
    }

    @PostMapping("getSensorData")
    public Result getSensorData(HttpServletRequest request) {
        String phone = OuyangUtils.getPhoneFromRequest(request);
        if(phone==null){
            return new Result(ResultEnum.tokenExpired);
        }
        List<Map<String, Object>> lists = sensorService.getSensorData(phone);
        return new Result(ResultEnum.success, lists);
    }


}
