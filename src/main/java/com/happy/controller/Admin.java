package com.happy.controller;

import com.happy.entity.Result;
import com.happy.entity.ResultEnum;
import com.happy.service.AdminService;
import com.happy.utils.OuyangUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/admin/")
public class Admin {

    private final AdminService adminService;

    public Admin(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("getToken") //管理员登录
    public Result getToken2(@RequestParam String phone, @RequestParam String password, HttpServletRequest request) {
        if(OuyangUtils.isEmptyOrNull(password)){
            return new Result(ResultEnum.paramNull);
        }
        if(!OuyangUtils.isPhoneNum(phone)){
            return new Result(ResultEnum.phoneFormatError);
        }
        if (adminService.getToken(phone, password)) {
            String loginInfo = phone+"_"+System.currentTimeMillis();
            request.getSession().setAttribute("admin", loginInfo);//保存token到session
            log.info("管理员{} 已登录",phone);
            return new Result(ResultEnum.success);
        }
        return new Result(ResultEnum.authFailed);
    }

    @PostMapping("resetPasswd")  //管理员修改密码
    public String resetPasswd2(@RequestParam String newPassword, HttpServletRequest request) {
        String phone= OuyangUtils.getPhoneFromRequest(request);
        if(phone==null){
            return new Result(ResultEnum.tokenExpired).toString();
        }
        int i = adminService.resetPasswd(phone, newPassword);
        return i == 0 ? ResultEnum.serverError.toString() : ResultEnum.success.toString();
    }

    @PostMapping("getAllUsers") //获取所有用户
    public Result getAllUsers(){
        List<Map<String, Object>> lists = adminService.getAllUsers();
        return new Result(ResultEnum.success,lists);
    }

//    public Result addUser(String phone, String password,String token){
// zengjiayige 增加ige Interceptor？ 更号把
//    }
//    public Result delUser(){}

}
