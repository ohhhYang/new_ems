package com.happy.controller;

import com.happy.entity.Result;
import com.happy.entity.ResultEnum;
import com.happy.service.UserService;
import com.happy.utils.OuyangUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户登录、修改密码
 */
@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/user/")
public class User {
    //新版spring特性？？？不用Autowired居然也行了？ 那就这么写吧，反正效果出来了
    private final UserService userService;

    public User(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("getToken") //用户登录
    public Result getToken(@RequestParam String phone, @RequestParam String password,
                           HttpServletRequest request, HttpServletResponse response) {
        //下面几句 解决跨域问题，让浏览器接收跨域， 类上的@Cross那啥，是让服务器接收跨域
        String origin = request.getHeader("Origin");                           //这三行不能用那个拦截器
        response.setHeader("Access-Control-Allow-Origin", origin);            //因为登录界面需要无token访问
        response.setHeader("Access-Control-Allow-Credentials","true");   //所以不用那个拦截器的话，要自己设置response头信息
        if (OuyangUtils.isEmptyOrNull(phone)) {       //判空
            return new Result(ResultEnum.paramNull, "手机号不能为空");
        }
        if (!OuyangUtils.isPhoneNum(phone)) {      //正则匹配判断格式
            return new Result(ResultEnum.phoneFormatError);
        }
        if (userService.auth(phone, password)) {    //数据库校验
            String loginInofo = phone+"_"+System.currentTimeMillis();
            request.getSession().setAttribute("loginInfo", loginInofo);//保存登录状态到session
            log.info("loginInfo: {}",loginInofo);
            return new Result(ResultEnum.success);
        }
        return new Result(ResultEnum.authFailed);
    }
    @PostMapping("logOut")
    public Result logOut(HttpServletRequest request){
        userService.logOut(request);
        return new Result(ResultEnum.success);
    }

    @PostMapping("resetPasswd")  //重置密码。
    public Result resetPasswd(@RequestParam String newPassword, HttpServletRequest request) {
        //因为有拦截器，所以这里不用担心getSession拿到的session不一致
        String phone=OuyangUtils.getPhoneFromRequest(request);
        if(phone==null){
            return new Result(ResultEnum.tokenExpired);
        }
        int i = userService.resetPasswd(phone, newPassword);
        return i == 0 ? new Result(ResultEnum.serverError) : new Result(ResultEnum.success);
    }

    @PostMapping("getUserInfo") //获取用户信息
    public Result getUserInfo(HttpServletRequest request) {
        String phone=OuyangUtils.getPhoneFromRequest(request);
        if(phone==null){
            return new Result(ResultEnum.tokenExpired);
        }
        return new Result(ResultEnum.success,userService.getUserInfo(phone));
    }


}
