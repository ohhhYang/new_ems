package com.happy.service;

import com.happy.entity.Result;
import com.happy.entity.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Slf4j
@Service
public class SensorService {

    private final JdbcTemplate jdbcTemplate;
    public static final int SensorNotExist=1;
    public static final int SensorFreezed=2;
    private static final int Success=3;

    public SensorService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List getSensors(String phone) {
        String getSensors = "select ems_device_address from ems_device where ems_device_user_id=?";
        List<Map<String, Object>> lists;
        try{
            lists = jdbcTemplate.queryForList(getSensors, phone);
        }catch (Exception e){
            e.printStackTrace();
            log.info(ResultEnum.sqlError.toString());
            return null;
        }
        ArrayList<Object> items = new ArrayList<>();
        for(Map<String,Object> m:lists){
            for(String k:m.keySet()){
                Object item = m.get(k);
                items.add(item);
            }
        }
        return items;
    }

    public Result turnOn_OffSensor(String sensorAddr, String controlCode) {
        if(!(controlCode.equals("0")||controlCode.equals("1"))){  //判断操作码格式1/0
            return new Result(ResultEnum.controlCodeError);
        }
        String sql = "update ems_device set ems_device_status=? where ems_device_address=?";
        int i = jdbcTemplate.update(sql, controlCode, sensorAddr);
        return (i!=-1)?new Result(ResultEnum.success):new Result(ResultEnum.serverError);
    }

    public List<Map<String,Object>> getSensorData(String phone){
        String sql="select * from ems_data where ems_user_id =?";
        List<Map<String, Object>> lists=null;
        try{
            lists = jdbcTemplate.queryForList(sql, phone);
        }catch (Exception e){
            e.printStackTrace();
            log.error(ResultEnum.sqlError.toString());
        }
        return lists;
    }

    public int validateSensorAddr(String phone, String sensorAddr) {
        String checkStatus = "select ems_device_status from ems_device where ems_device_address=? and ems_device_user_id=?";
        List<Map<String, Object>> lists = jdbcTemplate.queryForList(checkStatus, sensorAddr,phone);
        if(lists.size()==0){  //如果传感器不存在；
            return SensorNotExist;
        }else if(lists.get(0).get("ems_device_status").equals("0")){  //传感器是否被冻结
            return SensorFreezed;
        }
        return Success;
    }
}
