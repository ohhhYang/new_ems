package com.happy.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AdminService {
    private final JdbcTemplate jdbcTemplate;
    public AdminService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean getToken(String phone, String password) {
        String authSql2 = "select ems_manager_id from ems_manager where ems_manager_id=? and ems_manager_password=?";
        List<Map<String, Object>> lists = jdbcTemplate.queryForList(authSql2, phone, password);
        System.out.println(lists.size());
        return lists.size() != 0;
    }
    public int resetPasswd(String phone, String password) {
        String sql = "update ems_manager set ems_manager_password=? where ems_manager_id=?";
        return jdbcTemplate.update(sql, password, phone);
    }

    public List<Map<String,Object>> getAllUsers() {
        String sql ="select * from ems_user";
        return jdbcTemplate.queryForList(sql);

    }
}
