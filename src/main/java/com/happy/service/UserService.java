package com.happy.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
@Slf4j
@Service
public class UserService {
    private final JdbcTemplate jdbcTemplate;

    public UserService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public boolean auth(String phone, String password) {
        String authSql = "select ems_user_id from ems_user where ems_user_id=? and ems_user_password=?";
        List<Map<String, Object>> lists =null;
        try{

            lists = jdbcTemplate.queryForList(authSql, phone, password);
        }catch (Exception e){
            e.printStackTrace();
            log.error("sql语句执行出错，联网了么？");
        }
        if(lists==null){
            return false;
        }
        System.out.println(lists.size());
        return lists.size() != 0;
    }

    public Map getUserInfo(String phone) {
        String sql = "select ems_user_name, ems_user_create_time, ems_user_update_time from ems_user where ems_user_id=?";
        Map<String, Object> map=null;
        try{
            map = jdbcTemplate.queryForMap(sql, phone);
        }catch (Exception e){
            e.printStackTrace();
            log.error("sql执行出错");
        }
        return map;
    }

    public int resetPasswd(String phone, String password){
        String sql = "update ems_user set ems_user_password=? where ems_user_id=?";
        int i = 0;
        try {
            i = jdbcTemplate.update(sql, password, phone);
            log.info("用户{} 更改了密码：{}",phone,password);
        }catch (Exception e){
            e.printStackTrace();
            log.error("sql执行出错");
        }
        return i;
    }


    public boolean logOut(HttpServletRequest request) {
        request.getSession().setAttribute("loginInfo",null);
        return true;

    }
}
